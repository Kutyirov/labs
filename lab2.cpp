#include <iostream>
using namespace std;
struct Node {
	int data;
	Node* next;
};
void printlist(Node* pf)
{
	Node* p = pf;
	while (p != nullptr) {
		cout << p->data << " ";
		p = p->next;
	}
}
void insert(Node* first, int n) {
	Node* p = first;
	while (p->next != nullptr) {
		p = p->next;
	}
	Node* a = new Node{ n,nullptr };
	p->next = a;
}
bool destroy(Node* &first, int n) {
	bool x = false;
	Node* p = first;
	while (first->data == n) {
		x = true;
		Node* p = first;
		first = first->next;
		delete p;
	}

	p = first;
	while (p->next->next != nullptr) {
		if (p->next->data == n) {
			x = true;
			Node* q = p->next;
			p->next = p->next->next;
			delete q;
		}
		if (p -> next -> next != nullptr) p = p->next;
	}
	if (p->next->data == n) {
		x = true;
		Node* q = p->next;
		delete q;
		p->next = nullptr;
	}
	return(x);
}

int main() {
	Node* a1 = new Node{ 1,nullptr };
	Node* a2 = new Node{ 1,nullptr };
	Node* a3 = new Node{ 3,nullptr };
	Node* a4 = new Node{ 4,nullptr };
	Node* a5 = new Node{ 1,nullptr };
	cout << a1 << endl << a2 << endl << a3 << endl << a4 << endl << a5 << endl;
	a1->next = a2;
	a2->next = a3;
	a3->next = a4;
	a4->next = a5;
	printlist(a1);
	cout << endl;
	insert(a1, 1);
	printlist(a1);
	Node* first = a1;
	destroy(first, 1);
	cout << endl;
	printlist(first);
	system("pause");
	return 0;
}
